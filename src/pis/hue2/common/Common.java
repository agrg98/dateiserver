package pis.hue2.common;

import pis.hue2.server.LaunchServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * 
 * @author Aakash Garg
 * @author Phi-Long Tran
 * Class with functions, which client and servers have in common
 */
public class Common {

	
	/**
	 * Writes and sends a message to the given socket
	 * @param s is the socket
	 * @param str is the message
	 */
	public void sendMessage(Socket s,String str) {
		//converts chars into bytes
		PrintWriter pr = null;
		try {
			pr = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.print("PrintWriter konnte nicht erstellt werden. Socket falsch?");
			e.printStackTrace();
		}
		pr.println(str);
		pr.flush();	
		
	}
	/**
	 * Returns the received message for the given socket
	 * @param s is the socket
	 * @return the message as String
	 */
	public String receiveMessage(Socket s) {
		//reads bytes and decodes them into charstreams
		InputStreamReader in = null;
		try {
			in = new InputStreamReader(s.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//reads charstreams and buffers chars
		BufferedReader bf = new BufferedReader(in);
		String str = null;
		try {
			str = bf.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return str;
	}
	/**
	 * Sends a file from a given src to the given socket
	 * @param s the given socket
	 * @param pfad is the source of the file which should be send
	 */
	public void sendFile(Socket s, String pfad) {
		//initialize the file
		try {
			FileInputStream fr = new FileInputStream(pfad);
			//sends the src as a message to the server 
			sendMessage(s, pfad);
			//create byte array to put the file as a bytestream into
			byte b[] = new byte[4000];
			//puts the file into the byte array
			fr.read(b, 0, b.length);
			//sends the byte array to the connected socket and converts the bytearr to the stream
			OutputStream os = s.getOutputStream();
			os.write(b,0,b.length);

			os.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Receives a file from a given socket and saves it in the given destination source
	 * @param s the given socket
	 * @param dest destination source as String
	 */
	public void receiveFile(Socket s, String dest) {
		//create byte array to put the file as a bytestream into
		byte b[] = new byte[4000];
		//adds only the name and type of the received upload path
		String src = receiveMessage(s);
		dest = dest+src.substring(src.lastIndexOf("\\")+1);
		try {
			//gets the file as a stream
			InputStream is = s.getInputStream();
			//sets the destination source of the file
			FileOutputStream fr = new FileOutputStream(dest);
			is.read(b,0,b.length);
			fr.write(b, 0, b.length);
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Lists all files in the destination folder of the server
	 * @param dest the folder in which the files are listed
	 */
	public String listFiles(String dest) {

		String stringfiles = new String();
		//Create a file object
		File f = new File(dest);
		//list files in dest into arr
		File[] files = f.listFiles();

		//Display the names of the files
		try{
			for(File c : files) {
			 stringfiles += c.getName() + " ";
			}
		}catch(Exception e) {
            System.err.println(e.getMessage()); 
        }
		return stringfiles;


	}
	/**
	 * Sends an object of any type to the socket
	 * @param s the socket which the object should be sent to
	 * @param o the object which the socket should reveive
	 */
	public void sendObject(Socket s, Object o) {
		//get the output stream of the socket
		try {
			OutputStream os = s.getOutputStream();
			//create a object output stream for the output stream to send the object through it
			ObjectOutputStream oos = new ObjectOutputStream(os);
			//send object to the socket
			oos.writeObject(o);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Object receiveObject(Socket s) {
		//get the input stream from the connected socket
		try {
			InputStream is = s.getInputStream();
			//Create a object inputstream so we can get the object from the inputstream
			ObjectInputStream ois = new ObjectInputStream(is);
			return ois.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	/**
	 * Send a bytestream to create a new file in given location
	 * @param s Socket sending stream
	 * @param dest name of the file
	 * @param b size of stream
	 */
	public void sendByteToDat(Socket s,String dest, byte[] b) {

		//adds only the name and type of the received upload path

		dest = LaunchServer.directory + "\\" + dest;
		try {

			//sets the destination source of the file
			FileOutputStream fr = new FileOutputStream(dest);

			fr.write(b, 0, b.length);
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	
	
	

}
