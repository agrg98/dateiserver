package pis.hue2.client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import pis.hue2.common.Instruction;
import pis.hue2.server.LaunchServer;
import java.io.File;


/**
 * Controls the functions in the
 * GUI
 */
public class Controller {

    @FXML private TextArea chatLog;
    @FXML private TextField clientPort;
    @FXML private TextField serverStatus;
    @FXML private TextField datText;
    private LaunchServer server = null;
    private LaunchClient client;
    private final int port = 8000;
    private boolean status = false;


    public void startServer(ActionEvent event){

            try {
                if (!status){
                    server = new LaunchServer(8000);
                    server.start();

                    chatLog.appendText("[Server] Online on port: 8000 \n");
                    serverStatus.setText("Server ON");
                    status = true;
                }else{
                    chatLog.appendText("Port 8000 already in use \n");
                }
            } catch(Exception e){
                e.printStackTrace();
            }



    }

    public void endServer(ActionEvent event) {
        server.endServer();
        server = null;
        chatLog.appendText("[Server] Offline \n");
        serverStatus.setText("Server OFF \n");
        status = false;
    }

    public void connectClient(ActionEvent event){
        checkPort();
    }

    public void listFiles(ActionEvent event){
        checkPort();

        try{
            this.client = new LaunchClient("localhost",Integer.parseInt(clientPort.getText()));
            chatLog.appendText(client.list());
        }catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public void sendFile(ActionEvent event){
        checkPort();

        File file = chooseFile();


        try {
            client = new LaunchClient("localhost", Integer.parseInt(clientPort.getText()));
            client.sendMessage(client.socket, Instruction.PUT.toString());
            chatLog.appendText(client.receiveMessage() + "\n");
            client.sendFile(client.socket, file.toString());
            chatLog.appendText(client.receiveMessage() + "\n");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void receiveFile(ActionEvent event){
        checkPort();
       File file = chooseFile();

        try {
            client = new LaunchClient("localhost", Integer.parseInt(clientPort.getText()));
            client.sendMessage(client.socket, Instruction.GET.toString());
            chatLog.appendText(client.receiveMessage() + "\n");
            client.sendMessage(client.socket, file.toString());
            client.receiveFile(client.socket, LaunchServer.directory + "\\");
            chatLog.appendText("Send " +  " \n");
        }catch (Exception e){
            e.printStackTrace();


        }


    }

    /**Choose file from local directory to be
     * deleted
     */
    public void deleteFile() {
        checkPort();
        File file = chooseFile();

        try {
            client = new LaunchClient("localhost", Integer.parseInt(clientPort.getText()));
            client.sendMessage(client.socket, Instruction.DEL.toString());
            chatLog.appendText(client.receiveMessage() + "\n");
            client.sendMessage(client.socket, file.toString());
            chatLog.appendText(client.receiveMessage() + "\n");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get text from TextField, create a file with that name
     */
    public void DATbutton(ActionEvent event){
        checkPort();
        String dest = null;
        if(datText.getText().isEmpty()){
            chatLog.appendText("Please enter a file name \n");
            return;
        }else{
            dest = datText.getText();
        }
        byte[] file = new byte[8];

        try {
            client = new LaunchClient("localhost", Integer.parseInt(clientPort.getText()));
            client.sendMessage(client.socket, Instruction.DAT.toString());
            chatLog.appendText(client.receiveMessage() + "\n");
            client.sendMessage(client.socket, dest);
            client.sendObject(client.socket, file);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /** Checking for validity of the port before trying to connect with server
     * Not valid: letters, server status OFF, socket port not matching server port
     * */
    public void checkPort(){
        chatLog.appendText("[Client] Connecting.." + "\n");
        String text = clientPort.getText();

        if(text.matches("[0-9]+")) {
            if (server != null) {
                if (Integer.parseInt(clientPort.getText()) == port) {
                    try {
                        this.client = new LaunchClient("localhost", Integer.parseInt(clientPort.getText()));
                        client.sendMessage(client.socket, Instruction.CON.toString());
                        chatLog.appendText(client.receiveMessage() + " " + client.socket.getLocalSocketAddress() + "\n");
                    } catch (Exception e) {
                        e.printStackTrace();
                        chatLog.appendText("[Client] No Server on port " + clientPort.getText() + " to connect to" + "\n");

                    }
                    chatLog.appendText("[Client] Connected." + "\n");
                }else{
                    chatLog.appendText("Wrong port. Server is online on " + port + "\n");

                }
            } else {
                chatLog.appendText("Server is not online \n");

            }
        }else{
            chatLog.appendText("Not a valid port. Port may only contain numbers (Example: 4999) \n");

        }
    }

    /**
     * Choose a .txt file from local directory to send
     */
    public File chooseFile(){
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        Window primaryStage = null;
        File file = fileChooser.showOpenDialog(primaryStage);
        return file;
    }

    /**
     * Choose directory where files get sent and saved in
     */
    public void setDirectory(){
        DirectoryChooser dirChooser = new DirectoryChooser();
        Window primaryStage = null;
        File dir = dirChooser.showDialog(primaryStage);
        LaunchServer.directory = dir.toString();
        chatLog.appendText(dir.toString() + "\n");
        chatLog.appendText(LaunchServer.directory + "\n");

    }
}
