package pis.hue2.client;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Application;

/**
 * Connects to fxml file, main class for GUI
 */
public class ClientGUI extends Application{

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ClientGUI.fxml"));


        Scene scene = new Scene(root, 1280, 720);

        stage.setTitle("FXML Welcome");
        stage.setScene(scene);
        stage.show();
    }



}
