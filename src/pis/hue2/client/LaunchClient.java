package pis.hue2.client;

import java.io.IOException;
import java.net.Socket;
import pis.hue2.common.Common;
import pis.hue2.common.Instruction;


/**
 * @author Aakash Garg
 * @author Phi-Long Tran
 */
public class LaunchClient extends Common{
	Socket socket;
	

	/**
	 * Initializes the client, connects it to the server and sends CON msg
	 * @param host is the name of the host as a String
	 * @param port is the port nr it should get connected to
	 */
	public LaunchClient(String host, int port){
		try {
			this.socket = new Socket(host,port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public Socket getSocket() {
		return this.socket;
	}
	/**
	 * Returns the msg received by the server
	 * @override
	 * @return "Server + overridden msg"
	 */
	public String receiveMessage() {
		return "Server: " + super.receiveMessage(socket);
		
	}
	/**
	 * Requests the server to list all saved files
	 */
	public String list() {
		String list;
		sendMessage(socket, Instruction.LST.toString());
		list = receiveMessage();
		return list;

	}
	/**
	 * Upload a file to the server
	 * @param src of the file 
	 */
	public void put(String src) {
		sendMessage(socket, Instruction.PUT.toString());
		sendFile(socket, src);
	}
	/**
	 * Downloads the wanted file from the server
	 * @param file the name.type of the file
	 * @param dest the destination src in which the file should be downloaded
	 */
	public void get(String file, String dest) {
		sendMessage(socket, Instruction.GET.toString());
		sendMessage(socket, file);
		receiveFile(socket, dest);
	}
	/**
	 * Deletes a file from the server
	 * @param file the name.type of the file 
	 */
	public void del(String file) {
		sendMessage(socket, Instruction.DEL.toString());
		sendMessage(socket, file);
	}
	/**
	 * Disconnects from the server
	 */
	public void dsc() {
		sendMessage(socket, Instruction.DSC.toString());
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
