package pis.hue2.server;

import pis.hue2.common.Common;
import pis.hue2.common.Instruction;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * @author Akaash Garg
 * @author Phi-Long Tran
 *
 * Receives sockets from the server to be handled
 * in a thread
 */
public class ClientHandler extends Common implements Runnable {
    private final Socket client;

    public ClientHandler(Socket client){
        this.client = client;
    }

    /**
     * Thread executes task according to instruction sent by socket
     */
    @Override
        public void run() {
        try {
            String message = receiveMessage(client);
            //Sends ACK if the socket connected successfully
            if (message.equals(Instruction.CON.toString())) {
                sendMessage(client, Instruction.ACK.toString());
            }
            //Lists the files in the server
            else if (message.equals(Instruction.LST.toString())) {
                //listFiles("C:\\Users\\Aakas\\Desktop\\dateiserver\\src\\pis\\hue2\\server\\");
                sendMessage(client, listFiles(LaunchServer.directory));

            }
            //saves the uploaded file in the server
            else if (message.equals(Instruction.PUT.toString())) {
                //receiveFile(client, "C:\\Users\\Aakas\\Desktop\\dateiserver\\src\\pis\\hue2\\server\\");
                sendMessage(client, "File transfer accepted.");
                receiveFile(client, LaunchServer.directory + "\\");
                sendMessage(client, "File received.");
            }
            //sends the requested file by the socket
            else if (message.equals(Instruction.GET.toString())) {
                sendMessage(client, "File requested..");
                String datei = receiveMessage(client);
                //sendFile(client, "C:\\Users\\Aakas\\Desktop\\dateiserver\\src\\pis\\hue2\\server\\" + datei);
                sendMessage(client, "Transfering file " + datei);
                sendFile(client, datei);

            }
            //deletes the chosen file
            else if (message.equals(Instruction.DEL.toString())) {
                sendMessage(client, "Deletion request..");
                String datei = receiveMessage(client);
                File f = new File(datei);
                f.delete();
                sendMessage(client, "File " + f + " deleted");
            }
            // was macht das?
            else if (message.equals(Instruction.DAT.toString())) {
                sendMessage(client, "Creating new File...");
                String dest = receiveMessage(client);
                Object file = receiveObject(client);
                sendByteToDat(client, dest,(byte[]) file);
            }
            //closes the socket
            else if (message.equals(Instruction.DSC.toString())) {
                sendMessage(client, Instruction.DSC.toString());
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }finally {
            LaunchServer.maxCount--;
        }
    }
}


