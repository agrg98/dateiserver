package pis.hue2.server;



import java.io.File;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

import pis.hue2.common.Common;
import pis.hue2.common.Instruction;

/**
 * Starts server, waits for connections
 * and passes the connecting sockets to
 * another thread for handling
 */
public class LaunchServer extends Common {
	/**
	 * @param done breaks while-loop when endServer() is called
	 * @param maxCount keeps track of Socket connections, sends DND when more than 3 connect simultaneously
	 * @param directory default directory for testing purposes, can be changed in the GUI
	 */
	private ServerSocket server;
	private boolean done = false;
	private Semaphore maxConnections = new Semaphore(3);
	protected static int maxCount = 4;
	public static String directory = "C:\\Users\\Phi-Long Tran\\IdeaProjects\\HÜ2\\src\\pis\\hue2\\server\\";

	
	//create ServerSocket at Port 4999 for Clients to connect to
	public LaunchServer(int port) {
		try {
			this.server = new ServerSocket(port);
		} catch (BindException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**start server, pass connecting sockets to a new thread until EndServer() is called*/
	public void start() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(!done) {
					//waiting for client to connect
					Socket client = null;
					try {

						client = server.accept();
						maxCount--;
						if(maxCount == 0){
							sendMessage(client, Instruction.DND.toString());
							client.close();
						}else {
							ClientHandler handler = new ClientHandler(client);
							Thread t1 = new Thread(handler);
							t1.start();
						}

					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		}).start();

		
	}

	public void endServer(){
		try {
			done = true;
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	
	

}
